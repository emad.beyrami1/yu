package main

import "fmt"

type orderStatus int

const (
	none orderStatus = iota
	new
	received
	reserved
	filled
)

type order struct {
	ProductCode int
	Quantity    float64
	Status      orderStatus
}

// var orders []order

func (o order) String() string {
	return fmt.Sprintf("Product code: %v, Quantity: %v, Status: %v\n",
		o.ProductCode, o.Quantity, orderStatusToText(o.Status))
}

type invalidOrder struct {
	order order
	err   error
}

func orderStatusToText(o orderStatus) string {
	switch o {
	case none:
		return "none"
	case new:
		return "new"
	case received:
		return "received"
	case filled:
		return "filled"
	case reserved:
		return "reserved"
	default:
		return "unknown status"
	}
}
